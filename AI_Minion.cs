using UnityEngine;
using System.Collections;

public class AI_Minion : MonoBehaviour {
	
	private float _idleTime = 2f;
	[SerializeField]
	private Vector3 _target;
	[SerializeField]
	private bool _aggro = false;
	private bool _idling = true;
	private float _damage = 250f;
	
	private float _speed = 2.5f;
	
	private Vector3 _originPoint;
	private float _maxDeviation = 5f;
	private bool _coroutine = false;
	private float _lastHealth = 0f;
	private float _meleeRange = 2f;
	private bool _attacking = false;
	
	public bool Aggro {
		set {_aggro = value;}
		get { return _aggro;}
	}
	
	
	// Use this for initialization
	void Start () {
		_originPoint = this.transform.position;
		_lastHealth = this.GetComponent<HealthComponent>().CurrentPercentage();
	}
	
	// Update is called once per frame
	void Update () {	
		if(_lastHealth != this.GetComponent<HealthComponent>().CurrentPercentage() && !_aggro) {
			InitiateAggro();
		} 
		
		if(!_aggro){
			if(!_coroutine) {
				StartCoroutine("RandomMove");
			} 
		} else if (!_coroutine && !_attacking) {
			StartCoroutine("Charge");
		}
		
		UpdateAnimation();
	}
	
	private void UpdateAnimation() {
		if (!_aggro) {
			if(!_idling) {
				this.animation.Play (FindAnimation("Walk"));
			} else {
				this.animation.Play (FindAnimation("Idle"));
			}
		} else {
			if (!_attacking){
				this.animation.Play (FindAnimation("Run"));
			} 
		}
	}
	
	private IEnumerator Charge() {
		_coroutine = true;
		_target = GameObject.Find ("Player").transform.position;
       	var direction = (_target - this.transform.position).normalized;
       	var lookRotation = Quaternion.LookRotation(direction);
		while (Vector3.Distance(this.transform.position, _target) > _meleeRange) {
			_target = GameObject.Find ("Player").transform.position;
			direction = (_target - this.transform.position).normalized;
       		lookRotation = Quaternion.LookRotation(direction);
			this.transform.position = Vector3.MoveTowards(this.transform.position, _target, _speed*2*Time.deltaTime);
			this.transform.rotation = Quaternion.Slerp(this.transform.rotation, lookRotation, 0.2f);
			yield return 0;
		}
		if(!_attacking)
			StartCoroutine("Punch");
	}
	
	private IEnumerator Punch() {
		_attacking = true;
		StopCoroutine("Charge");
		this.animation.Play (FindAnimation("Punch"));
		var direction = (_target - this.transform.position).normalized;
       	var lookRotation = Quaternion.LookRotation(direction);
		var damageDone = false;
		while (this.animation.IsPlaying (FindAnimation("Punch"))) {
			_target = GameObject.Find ("Player").transform.position;
			direction = (_target - this.transform.position).normalized;
       		lookRotation = Quaternion.LookRotation(direction);
			this.transform.rotation = Quaternion.Slerp(this.transform.rotation, lookRotation, 0.2f);
			if (!damageDone)
				damageDone = UpdateAttack();
			yield return 0;
		}
		yield return 0;
		_attacking = false;
		yield return 0;
		_coroutine = false;
	}
	
	bool UpdateAttack() {
		foreach (Collider collider in this.GetComponentsInChildren<BoxCollider>()) {
			if (collider.tag == "Melee") {
				if (collider.bounds.Intersects(GameObject.Find ("Player").collider.bounds)) {
					//GameObject.Find ("Player").GetComponent<Player>().AddForce();
					GameObject.Find ("Player").GetComponent<HealthComponent>().DoDamage(_damage);
					return true;
				}
			}
		}
		return false;
	}
	
	private IEnumerator RandomMove() {
		_coroutine = true;
		_idling = false;
		_target = RandomTarget();
       	var direction = (_target - this.transform.position).normalized;
       	var lookRotation = Quaternion.LookRotation(direction);
		while (this.transform.position != _target) {
			this.transform.position = Vector3.MoveTowards(this.transform.position, _target, _speed*Time.deltaTime);
			this.transform.rotation = Quaternion.Slerp(this.transform.rotation, lookRotation, 0.2f);
			yield return 0;
		}
		_idling = true;
		yield return new WaitForSeconds(_idleTime);
		_coroutine = false;
	}
	
	private Vector3 RandomTarget() {
		float xDeviation = Random.Range (-_maxDeviation, _maxDeviation);
		float zDeviation = Random.Range (-_maxDeviation, _maxDeviation);
		return new Vector3(_originPoint.x + xDeviation, _originPoint.y, _originPoint.z + zDeviation);
	}
	
	private string FindAnimation(string name) {
		foreach (AnimationState clip in this.animation) {
			if (clip.name.Contains (name))
				return clip.name;
		}
		return null;
	}
	
	public void InitiateAggro() {
		StopCoroutine ("RandomMove");
		_coroutine = false;
		_aggro = true;
		_target = GameObject.Find ("Player").transform.position;
	}
}
