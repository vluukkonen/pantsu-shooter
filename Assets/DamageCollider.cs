using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Collider))]
public class DamageCollider : MonoBehaviour {
	
	[SerializeField]
	private AI_Base _parent;
	
	private void Start() {
		this.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
	}
	
	private void OnTriggerStay(Collider other) {
		if (_parent.Attacking) {
			HealthComponent health = other.GetComponent<HealthComponent>();
			if (health != null)
				health.DoDamage(_parent.Damage);
		}
	}
}
