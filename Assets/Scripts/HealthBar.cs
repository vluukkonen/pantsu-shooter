using UnityEngine;
using System.Collections;

public class HealthBar : MonoBehaviour {
	[SerializeField]
	private HealthComponent _health;
	[SerializeField]
	private Texture _foregroundTexture;
	[SerializeField]
	private Texture _backgroundTexture;
	[SerializeField]
	private Texture2D _damageTexture;
	[SerializeField]
	private Texture _portrait;
	[SerializeField]
	private bool _rightSide = false;
	
	void OnGUI () {
		
		// Make rect 10 pixels from side edge and 6 pixels from top. 
		Rect rect = new Rect(120, 50, 300, _backgroundTexture.height);
		Rect port = new Rect(10, 0, 150, 150);
		// If this is a right side health bar, flip the rect.
		if (_rightSide) {
			rect.x = Screen.width - rect.x;
			rect.width = -rect.width;
		}
		
		// Draw the background texture
		GUI.DrawTexture(rect, _backgroundTexture);
		GUI.DrawTexture(port, _portrait);
		float health = _health.CurrentPercentage;
		
		// Multiply width with health before drawing the foreground texture
		rect.width *= health;
		
		// Get color from damage texture
		GUI.color = _damageTexture.GetPixelBilinear(health, 0.5f);
		
		// Draw the foreground texture
		GUI.DrawTexture(rect, _foregroundTexture);
		
		// Reset GUI color.
		GUI.color = Color.white;
	}
	
}















