using UnityEngine;
using System.Collections;

public class AI_Fast : AI_Base {
	
	[SerializeField]
	private float _circlingRange = 10f;
	[SerializeField]
	private float _circlingDegrees = 180f;
	
	protected override IEnumerator Charge() {
		_coroutine = true;
		
		_target = GameObject.Find ("Player");
       	var direction = (_target.transform.position - this.transform.position).normalized;
       	var lookRotation = Quaternion.LookRotation(direction);
		while (Vector3.Distance(this.transform.position, _target.transform.position) > _circlingRange) {

			direction = (_target.transform.position - this.transform.position).normalized;
       		lookRotation = Quaternion.LookRotation(direction);
			
			Vector3 target = Vector3.MoveTowards(this.transform.position, _target.transform.position, _speed*Time.deltaTime);
			target.y = this.transform.position.y;
			this.transform.position = target;
			
			Quaternion newRot = Quaternion.Slerp(this.transform.rotation, lookRotation, 0.2f);
			newRot.x = 0f;
			newRot.z = 0f;
			this.transform.rotation = newRot;
			
			yield return 0;
		}
		
		if(!_attacking)
			StartCoroutine(CircleTarget());
	}
	
	private IEnumerator CircleTarget() {
		float distance = _circlingRange;
		float degrees = 0;
		
		while (distance > _attackRange) {
			degrees += Mathf.PI*_speed*Time.deltaTime;
			this.transform.position = Vector3.MoveTowards(this.transform.position, GetUnitOncircle(degrees, distance, _target.transform.position), _speed*Time.deltaTime);
			distance -= _speed/2*Time.deltaTime;
			base.LookAtTarget();
			yield return 0;
		}
		
		Debug.Log ("Circling done");
		
		if(!_attacking) {
			StartCoroutine(Attack()); 
			Debug.Log ("Attacking");
		}
	}
	
	protected override IEnumerator Attack() {
		base.Attack();
		base.LookAtTarget();
		StartCoroutine(Back());
		yield return 0;
	}
	
	private IEnumerator Back() {
		_coroutine = true;
		_attacking = false;
		Vector3 target = this.transform.position - this.transform.forward*(_circlingRange+_attackRange);
		while (Vector3.Distance(this.transform.position, target) > 0.5f) {
			this.transform.position = Vector3.MoveTowards(this.transform.position, target, _speed*2*Time.deltaTime);
			yield return 0;
		}
		_coroutine = false;
	}
	
	private Vector3 GetUnitOncircle(float angleDegrees, float radius, Vector3 pointToCircle) {
	    // initialize calculation variables
	    float x = pointToCircle.x;
		float y = pointToCircle.y;
	    float z = pointToCircle.z;
	    float angleRadians = 0;
	 
	    // convert degrees to radians
	    angleRadians = angleDegrees * Mathf.PI / 180.0f;
	 
	    // get the 2D dimensional coordinates
	    x += radius * Mathf.Cos(angleRadians);
	    z += radius * Mathf.Sin(angleRadians);
	 
	    // return the vector info
	    return new Vector3(x, y, z);
	}
}
