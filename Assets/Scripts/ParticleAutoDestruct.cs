using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleAutoDestruct : MonoBehaviour {
	void Update () {
		if (!this.particleSystem.isPlaying)
			GameObject.DestroyImmediate (this.gameObject, true);
	}
}
