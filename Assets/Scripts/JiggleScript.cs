using UnityEngine;
using System.Collections;

//
// Jiggle Bone (Simple version)
//   -  by WarpZone
//
//  TO USE:
//  Simply attach this script to any Transform in your model's hierarchy.
//
//  FEATURES:
//    -  It doesn't matter what the forward normal of your bone is, it just works.
//    -  Bounce and sway can be configured independently
//    -  Simple script is Simple.
//
// If you want to call the bounce code using some sort of manager script, comment out the JiggleBonesUpdate call and SendMessage JiggleBonesUpdate from your Manager instead.  
// This is useful if you want to control which bones update first, or strategically layer other procedural animation effects at runtime.
//
	
//#pragma strict

public class JiggleScript : MonoBehaviour {
	
	[SerializeField]
	private float bounceFactor = 20f;
	[SerializeField]
	private float wobbleFactor = 10f;
	[SerializeField]
	private float maxTranslation = 0.05f;
	[SerializeField]
	private float maxRotationDegrees = 5f;
		
	private Vector3 oldBoneWorldPosition;
	private Quaternion oldBoneWorldRotation;
	
	private Vector3 animatedBoneWorldPosition;
	private Quaternion animatedBoneWorldRotation;
	
	private Vector3 goalPosition;
	private Quaternion goalRotation;
	
	void Awake(){
	    oldBoneWorldPosition = transform.position;
	    oldBoneWorldRotation = transform.rotation;
	}
	
	void LateUpdate () {
	    JiggleBonesUpdate();
	}
	
	private void JiggleBonesUpdate(){
	    //Mesh has just been animated
	    animatedBoneWorldPosition = transform.position;
	    animatedBoneWorldRotation = transform.rotation;
	    goalPosition = Vector3.Slerp(oldBoneWorldPosition, transform.position, Time.deltaTime * bounceFactor);
	    goalRotation = Quaternion.Slerp(oldBoneWorldRotation, transform.rotation, Time.deltaTime * wobbleFactor);
	
	    transform.rotation = Quaternion.RotateTowards( animatedBoneWorldRotation, goalRotation , maxRotationDegrees );
	    transform.position = Vector3.MoveTowards( animatedBoneWorldPosition, goalPosition , maxTranslation );
	
	    oldBoneWorldPosition = transform.position;
	    oldBoneWorldRotation = transform.rotation;
	}
}
