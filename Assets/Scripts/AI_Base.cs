using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof (HealthComponent))]
[RequireComponent (typeof (AudioSource))]
public class AI_Base : MonoBehaviour {
	
	[SerializeField]
	protected bool _aggro = false;
	[SerializeField]
	protected float _attackRange = 2f;
	[SerializeField]
	protected float _damage = 250f;
	[SerializeField]
	protected float _idleTime = 2f;
	[SerializeField]
	protected float _speed = 2.5f;
	[SerializeField]
	protected GameObject _target;
	
	[SerializeField]
	protected List<string> _soundEffects; // Played on attack
	
	protected bool _attacking = false;
	protected bool _coroutine = false;
	protected bool _idling = true;
	protected bool _takingDamage = false;
	protected float _lastHealth = 0f;
	protected float _maxDeviation = 5f;
	protected HealthComponent _health;
	protected Vector3 _originPoint;
	
	public bool TakingDamage {
		get { return _takingDamage; }
	}
	
	public bool Attacking {
		get { return _attacking; }
	}
	
	public float Damage {
		get { return _damage; }
	}
	
	// Use this for initialization
	protected virtual void Start () {
		_originPoint = this.transform.position;
		_health = this.GetComponent<HealthComponent>();
		_lastHealth = _health.CurrentPercentage;
		foreach (string clip in _soundEffects)
			AudioManager.Instance.LoadSound(clip);
	}
	
	public void TakeDamage(float damage) {
		if (!_takingDamage) {
			_health.DoDamage(damage);
			_takingDamage = true;
		}
	}
	
	public GameObject Target {
		get { return _target; }
		set { _target = value; }
	}
	
	// Update is called once per frame
	protected virtual void Update () {	
		if(_lastHealth != _health.CurrentPercentage && !_aggro)
			InitiateAggro();
		if (!_coroutine) {
			if(!_aggro){
				StartCoroutine(RandomMove());
			} else if (!_attacking) {
				StartCoroutine(Charge());
			}
		}
		
		//if (_takingDamage) 
			//StartCoroutine(TakeDamage());
	}
	
	protected void PlayRandomSound() {
		if (_soundEffects.Count > 0) {
			int sound = Random.Range (0, _soundEffects.Count);
			this.audio.clip = AudioManager.Instance.GetSound (_soundEffects[sound]);
			this.audio.Play ();
		}
	}
	
	protected virtual IEnumerator Charge() {
		_coroutine = true;
		
		_target = GameObject.Find ("Player");
       	var direction = (_target.transform.position - this.transform.position).normalized;
       	var lookRotation = Quaternion.LookRotation(direction);
		while (Vector3.Distance(this.transform.position, _target.transform.position) > _attackRange) {

			LookAtTarget();
			
			direction = (_target.transform.position - this.transform.position).normalized;
			Vector3 target = Vector3.MoveTowards(this.transform.position, _target.transform.position, _speed*Time.deltaTime);
			target.y = this.transform.position.y;
			this.transform.position = target;
			
			yield return 0;
		}
		
		if(!_attacking)
			StartCoroutine(Attack());
	}
	
	protected virtual IEnumerator Attack() {
		_attacking = true;
		StopCoroutine("Charge");
		PlayRandomSound();
		LookAtTarget();
		yield return 0;
		_attacking = false;
		yield return 0;
		_coroutine = false;
	}
	
	protected virtual IEnumerator RandomMove() {
		_coroutine = true;
		_idling = false;
		var target = RandomTarget();
       	var direction = (target - this.transform.position).normalized;
       	var lookRotation = Quaternion.LookRotation(direction);
		while (this.transform.position != target) {
			this.transform.position = Vector3.MoveTowards(this.transform.position, target, _speed*Time.deltaTime);
			this.transform.rotation = Quaternion.Slerp(this.transform.rotation, lookRotation, 0.2f);
			yield return 0;
		}
		_idling = true;
		yield return new WaitForSeconds(_idleTime);
		_coroutine = false;
	}
	
	protected Vector3 RandomTarget() {
		float xDeviation = Random.Range (-_maxDeviation, _maxDeviation);
		float zDeviation = Random.Range (-_maxDeviation, _maxDeviation);
		return new Vector3(_originPoint.x + xDeviation, _originPoint.y, _originPoint.z + zDeviation);
	}
	
	protected void LookAtTarget() {
			var direction = (_target.transform.position - this.transform.position).normalized;
			var lookRotation = Quaternion.LookRotation(direction);
			Quaternion newRot = Quaternion.Slerp(this.transform.rotation, lookRotation, 0.2f);
			newRot.x = 0f;
			newRot.z = 0f;
			this.transform.rotation = newRot;
	} 
	
	public void InitiateAggro() {
		StopCoroutine ("RandomMove");
		_aggro = true;
		_target = GameObject.Find ("Player");
	}
}
