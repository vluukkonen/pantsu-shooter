using UnityEngine;
using System.Collections;

public class PantsuAvatarController : MonoBehaviour {

	[SerializeField]
	private Transform _spine;
	[SerializeField]
	private PantsuCharacterMotor _motor;
	[SerializeField]
	private Weapon _weapon;
	[SerializeField]
	private Animator _animator;
	
	private bool _aiming = false;
	private float _speed = 0;
	
	private void Start () {
		_animator.SetLayerWeight (1, 1f);
		_animator.SetBool("Reloading", _weapon.Reloading);
	}
	
	private void Update () {
		_speed = Mathf.Abs (Input.GetAxis ("Vertical")) + Mathf.Abs (Input.GetAxis ("Horizontal"));	
		
		if (Input.GetAxis ("Vertical") < 0)
			_speed = -_speed;
		
		if (Input.GetButtonDown("Fire2") || Input.GetButtonDown("Fire1")) {
			_aiming = true;
		} else if (Input.GetButtonUp("Fire2")){
			_aiming = false;
		}
		
		if (Input.GetButtonDown("Jump")) {
			_motor.Jump();
		}
		
		if (Input.GetKey(KeyCode.Z)) {
			_animator.SetBool("Reloading", _weapon.Reloading);
			_aiming = false;
			
		}
		
		_animator.SetBool("Aiming", _aiming);
		
		_animator.SetFloat ("Speed", _speed);
		_animator.SetBool("Jumping", _motor.jumping);
		
		
		UpdateSpineRotation();
		
		Debug.Log ("Reloading:" + _weapon.Reloading);
		Debug.Log ("Aiming:" + _aiming);
	}
	
	private void UpdateSpineRotation() {
		_spine.rotation = Quaternion.LookRotation (this.transform.forward); //Camera.mainCamera.transform.forward * 1000f);
		
		if (_aiming || _weapon.Reloading)
			_spine.Rotate (0f, 45f, -90f);
		else 
			_spine.Rotate (0f, 0f, -90f);
	}
}
