using UnityEngine;
using System.Collections;


[RequireComponent (typeof(AudioSource))]
public class Weapon : MonoBehaviour {
	[SerializeField]
    private GameObject _projectile;
	[SerializeField]
	private float _cycleTime = 0.2f;
	[SerializeField]
	private float _effectiveRange = 1000f;
	[SerializeField]
	private Transform _muzzle;
	[SerializeField]
	private AudioClip _fireSound;
	[SerializeField]
	private AudioClip _reloadSound;
	
	[SerializeField]
	private int _magazineCapacity = 30;
	[SerializeField]
	private int _rounds = 30;
	
	private float _cooldown = 0f;
	private bool _reloading = false;
	
	public bool Reloading {
		get { return _reloading; }
	}
	
	private void Start() {
		this.audio.clip = _reloadSound;
	}
	
	private void LateUpdate () {
		_cooldown -= Time.deltaTime;
		
        if (Input.GetButton("Fire1") && !_reloading && _rounds > 0) {
			Screen.lockCursor = true;
			
			if (_cooldown < 0) {
				Vector3 spawnPos = (_muzzle != null) ? _muzzle.position : transform.position ;
				Quaternion rotation = this.transform.rotation;
				rotation.SetLookRotation(Camera.mainCamera.transform.forward * _effectiveRange);
				
	           	GameObject go = GameObject.Instantiate(_projectile, spawnPos, rotation) as GameObject;
				this.GetComponentInChildren<ParticleSystem>().Play();
				AudioSource.PlayClipAtPoint(_fireSound, _muzzle.position);
				
				_cooldown = _cycleTime;
				_rounds--;
			}
        }
		
		if (Input.GetKey(KeyCode.Z) && !_reloading) {
			StartCoroutine (Reload ());
		}
	}
	
	private IEnumerator Reload() {
		_reloading = true;
		this.audio.Play ();
		float time = 0;
		while (time < audio.clip.length) {
			yield return 0;
			time += Time.deltaTime;
		}
		_rounds = _magazineCapacity;
		_reloading = false;
	}
}
