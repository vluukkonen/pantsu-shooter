using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof (AudioSource))]
public class HealthComponent : MonoBehaviour {

	[SerializeField]
	private float _health = 1500;
	[SerializeField]
	private List<string> _soundEffects;
	[SerializeField]
	private float _damageProtectTime = 0f;
	
	private float _maxHealth;
	private float _damageProtectTimeCounter = 0f;
	
	void PlayRandomSound() {
		if (_soundEffects.Count > 0) {
			int sound = Mathf.RoundToInt (Random.Range (0f, (float)(_soundEffects.Count-1)));
			this.audio.clip = AudioManager.Instance.GetSound (_soundEffects[sound]);
			this.audio.Play ();
		}
	}
	
	// Use this for initialization
	void Start () {
		_maxHealth = _health;
		foreach (string clip in _soundEffects)
			AudioManager.Instance.LoadSound(clip);
	}
	
	public int CurrentPercentage {
		get { return Mathf.Clamp((int)((_health/_maxHealth)*100), 0, 100); }
	}
	
	public void DoDamage(float damage) {
		if (_damageProtectTimeCounter <= 0) {
			if (_health > 0) {
				_damageProtectTimeCounter = _damageProtectTime;
				PlayRandomSound ();
				this._health -= damage;
				if (_health > _maxHealth)
					_health = _maxHealth;
				if (_health <= 0)
					Die();
			}
		}
	}
	
	private void Die() {
		//DestroyImmediate (this.gameObject, true);
	}
	
	private void ToggleColliderTriggers() {
		foreach (BoxCollider collider in this.gameObject.GetComponentsInChildren<BoxCollider>()) {
			collider.isTrigger = !collider.isTrigger;
		}
	}
	
	private string FindAnimation(string name) {
		foreach (AnimationState clip in this.animation) {
			if (clip.name.Contains (name))
				return clip.name;
		}
		return null;
	}
	
	private void Update() {
		if (_damageProtectTimeCounter > 0)
			_damageProtectTimeCounter -= Time.deltaTime;
	}
}
