using UnityEngine;
using System.Collections;

public class ProjectileController : MonoBehaviour {
	
	[SerializeField]
	private float _damage = 0;
	[SerializeField]
	private int _velocity = 0;
	[SerializeField]
	private GameObject _hitParticles;
	
	private RaycastHit _hit;
	private Vector3 _currPos;
	private Vector3 _prevPos;
	
	// Use this for initialization
	private void Start () {
		_prevPos = this.transform.position;
		this.rigidbody.AddForce((this.transform.forward * _velocity)*Time.deltaTime);
	}
	
	private void Update() {
		_currPos = this.transform.position;
		if(_prevPos != null && _currPos != _prevPos) {
			
			if(Physics.Raycast(_prevPos, this.transform.forward, out _hit, Vector3.Distance (_prevPos, _currPos))) { 
				
				HealthComponent health = _hit.collider.GetComponent<HealthComponent>();
				if (health != null)
					health.DoDamage (_damage);
				
				this.rigidbody.velocity = Vector3.zero;
				Debug.DrawRay(_hit.point, this.transform.up, Color.red, 5f);
				
				if(_hitParticles)
					GameObject.Instantiate(_hitParticles, transform.position, Quaternion.LookRotation(-this.transform.forward));
				
				GameObject.Destroy (this.gameObject);
			}
			
		}
		_prevPos = _currPos;
	}
	
	public Vector3 PrevPos {
		set { _prevPos = value; }
	}
	
}