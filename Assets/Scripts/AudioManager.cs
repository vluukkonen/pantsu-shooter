using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public sealed class AudioManager {
	
	private static readonly AudioManager _instance = new AudioManager();
	private readonly string MUSIC_DIRECTORY = "Audio/Music/";
	private readonly string SOUND_DIRECTORY = "Audio/SFX/";
	
	private Dictionary<string, AudioClip> _music;
	private Dictionary<string, AudioClip> _soundEffects;
	
	private AudioManager() {
		_music = new Dictionary<string, AudioClip>(1);
		_soundEffects = new Dictionary<string, AudioClip>(5);
	}
	
	public static AudioManager Instance {
		get { return _instance; }
	}
	
	public AudioClip LoadSound(string filename) {
		if (_soundEffects.ContainsKey(filename)) return _soundEffects[filename];
		AudioClip clip = Resources.Load(SOUND_DIRECTORY + filename) as AudioClip;
		if (clip != null) {
			_soundEffects.Add (filename, clip);
			return clip;
		}
		return null;
	}
	
	public AudioClip LoadMusic(string filename) {
		if (_music.ContainsKey(filename)) return _music[filename];
		AudioClip clip = Resources.Load(MUSIC_DIRECTORY + filename) as AudioClip;
		if (clip != null) {
			_music.Add (filename, clip);
			return clip;
		}
		return null;
	}
	
	public AudioClip GetSound(string filename) {
		AudioClip clip;
		if (_soundEffects.TryGetValue (filename, out clip))
			return clip;
		else
			return LoadSound (filename);
	}
	
	public AudioClip GetMusic(string filename) {
		AudioClip clip;
		if (_music.TryGetValue (filename, out clip))
			return clip;
		else
			return LoadMusic (filename);
	}
}
