using UnityEngine;
using System.Collections;

public class HealthPickup : MonoBehaviour {
	
	public float amount = 10;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	 void OnTriggerEnter(Collider hit) {
		if(hit.gameObject.name == "Player") {
			hit.gameObject.SendMessage("OnHit", -amount, SendMessageOptions.DontRequireReceiver);	
			Destroy(gameObject);
		}
    }
}
