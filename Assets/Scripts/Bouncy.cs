using UnityEngine;
using System.Collections;

public class Bouncy : MonoBehaviour {
	
	Vector3 start;
	Vector3 end;
	
	// Use this for initialization
	void Start () {
		start = this.transform.position;
		end = this.transform.position + this.transform.up;
		Debug.Log (end);
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.position = Vector3.Slerp(start, end, 0.05f);
		if(Input.anyKeyDown) this.transform.position = start;
	}
}
