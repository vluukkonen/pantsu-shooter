using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {
	
	public float maxHealth = 100.0f;
	public float health;
	public float speed = 6.0f;
	public float damage = 10.0f;
	private Vector3 wayPoint;
		
	public GameObject player;
	public GameObject pickup;
	public GameObject deathAnimation;
	public GameObject hitParticles;
	 
	void Start(){
		health = maxHealth;
	   	//initialise the target way point
	   	Wander();
	}
	 
	void Update() 
	{
	   // this is called every frame
	   // do move code here
		transform.LookAt(wayPoint);
	   	transform.position += transform.TransformDirection(Vector3.forward) * speed * Time.deltaTime;
	    if((transform.position - wayPoint).magnitude < 3)
	    {
	        // when the distance between us and the target is less than 3
	        // create a new way point target
	        Wander(); 
	    }
		if(health <= 0) {
			if(deathAnimation) {
				Instantiate(deathAnimation, transform.position, transform.rotation);
			}
			if(pickup) {
				Instantiate(pickup, transform.position, transform.rotation);
			}
			Destroy(gameObject);
		}
	}
	 
	void Wander()
	{
	   // picks a new destination to go to
	    wayPoint= player.transform.position + Random.insideUnitSphere *10;
	    wayPoint.y = 0.5f;
	
	    
	    //Debug.Log(wayPoint + " and " + (transform.position - wayPoint).magnitude);
	}
	
 	void OnCollisionEnter(Collision hit) {
		if(hit.gameObject.name == "Player")
			hit.gameObject.SendMessage("OnHit", damage, SendMessageOptions.DontRequireReceiver);	

    }
	
	void OnHit (float hitDamage) {
			if(hitParticles) {
				GameObject particles = Instantiate(hitParticles, transform.position, transform.rotation) as GameObject;
				particles.transform.parent = transform;
			}
			health -= hitDamage;
			health = Mathf.Clamp(health, 0, maxHealth);


	}	
}
