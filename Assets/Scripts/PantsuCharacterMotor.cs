using UnityEngine;
using System.Collections;

public class PantsuCharacterMotor : CharacterMotor {
	[SerializeField]
	private float _maxRotation = 60f;
	
	private float _forwardSpeed = 0;
	private float _sidewaysSpeed = 0;
	private float _upSpeed = 0f;
	private float _rotation = 0;
	private Transform _viewPort;

	private void Start () {
		_viewPort = Camera.mainCamera.transform;
	}
	
	private void Update () {
		/*
		var rotation = Quaternion.LookRotation (Camera.mainCamera.transform.forward);
		rotation.Set(0, rotation.y, 0, rotation.w);
		_rotation = Mathf.Clamp (_maxRotation*Input.GetAxis ("Horizontal"), -_maxRotation, _maxRotation);
		
		if (Input.GetAxis ("Vertical") < 0) {
			_rotation = -_rotation;
		}
		
		rotation *= Quaternion.Euler (0f, _rotation, 0f);
		this.transform.rotation = rotation;
		*/
	}
	
	public void Jump() {
		if (!jumping && grounded) {
			jumping = true;
			_upSpeed = jumpHeight;
		}
	}
	
	private void LateUpdate () {
		Vector3 movement = Vector3.zero;
		Vector3 forward = _viewPort.forward;
		forward.y = 0;
		Vector3 right = _viewPort.right;
		right.y = 0;
		
		_forwardSpeed += (maxForwardSpeed * maxVelocityChange) * Input.GetAxis ("Vertical");
		_sidewaysSpeed += (maxSidewaysSpeed * maxVelocityChange) * Input.GetAxis ("Horizontal");
		
		_forwardSpeed = Mathf.Clamp(_forwardSpeed, -maxBackwardsSpeed, maxForwardSpeed);
		_sidewaysSpeed = Mathf.Clamp (_sidewaysSpeed, -maxSidewaysSpeed, maxSidewaysSpeed);
		
		movement += forward * _forwardSpeed;
		movement += right * _sidewaysSpeed;
		movement.y = _upSpeed;
		
		CollisionFlags flags = this.GetComponent<CharacterController>().Move (movement*Time.deltaTime);
		grounded = (flags & CollisionFlags.CollidedBelow) != 0;
		
		_forwardSpeed = EaseSpeed(Input.GetAxis ("Vertical"), _forwardSpeed); 
		_sidewaysSpeed = EaseSpeed(Input.GetAxis ("Horizontal"), _sidewaysSpeed);
		ApplyGravity ();
	}
	
	private void ApplyGravity() {
		if (!grounded) {
			_upSpeed -= gravity*Time.deltaTime;
		} else {
			_upSpeed = 0;
			jumping = false;
		}
	}
	
	private float EaseSpeed(float axis, float speed) {
		if (axis == 0) {
			if (Mathf.Abs (speed) < maxVelocityChange) {
				return speed = 0;
			}
			return speed *= maxVelocityChange;
		}
		return speed;
	} 
}
