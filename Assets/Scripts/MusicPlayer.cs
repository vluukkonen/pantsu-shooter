using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]
public class MusicPlayer : MonoBehaviour {
	
	[SerializeField]
	private string _musicToPlay;
	
	[SerializeField]
	private bool _loop;
	
	// Use this for initialization
	void Start () {
		AudioManager.Instance.LoadMusic (_musicToPlay);
	}
	
	// Update is called once per frame
	void Update() {
		if(!this.audio.isPlaying) {
			PlayMusic(_musicToPlay, _loop);
		}
	}
	
	public void PlayMusic(string filename, bool loop = false) {
		this.audio.Stop();
		if (filename != null) {
			this.audio.clip = AudioManager.Instance.GetMusic(filename);
		}
		this.audio.loop = loop;
		this.audio.Play();
	}
	
	
}